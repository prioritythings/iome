class CreateMessages < ActiveRecord::Migration[5.2]
  def up
    create_table :messages do |t|
      t.string :content, :limit => 180
      t.string :status, :limit => 20, :comment => "['backlog','done','ongoing','error','delayed','removed']"

      t.timestamps
    end
  end

  def down
    drop_table :messages
  end
end
