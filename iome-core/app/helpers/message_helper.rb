module MessageHelper
  STATUS_CLASSES = {
      :backlog => 'alert-dark',
      :done => 'alert-success',
      :ongoing => 'alert-warning',
      :error => 'alert-danger',
      :delayed => 'alert-secondary',
      :removed => 'alert-light'
  }
  def allowed_statuses
    Message.statuses.values.join(', ')
  end

  def status_to_css_class(status)
    return 'alert-primary' if status.nil?
    STATUS_CLASSES.key?(status.to_sym) ? STATUS_CLASSES[status.to_sym] : 'alert-primary'
  end
end