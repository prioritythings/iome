module ApplicationHelper
  def custom_paginate_render
    # https://github.com/mislav/will_paginate/blob/master/lib/will_paginate/view_helpers/link_renderer.rb
    # https://stackoverflow.com/questions/44680975/custom-will-paginate-renderer
    Class.new(WillPaginate::ActionView::LinkRenderer) do
      def page_number(page)
        if page == current_page
          tag(:li, link(page, page, :rel => rel_value(page), :class => 'page-link'), :class => 'page-item active')
        else
          tag(:li, link(page, page, :rel => rel_value(page), :class => 'page-link'), :class => 'page-item')
        end
      end
      def previous_or_next_page(page, text, classname)
        if page
          tag(:li, link(text, page, :class => "page-link"), :class => "page-item #{classname}")
        else
          tag(:li, link(text, '#', :class => "page-link"), :class => "page-item #{classname} disabled")
        end
      end
    end
  end
  def bootstrap_paginate(collection)
    # https://github.com/mislav/will_paginate/wiki/Link-renderer
    # https://github.com/mislav/will_paginate/blob/master/lib/will_paginate/view_helpers.rb

    options = {
      :container => false,
      :renderer => custom_paginate_render
    }
    "<ul class=\"pagination\">#{will_paginate(collection, options)}</ul>".html_safe
  end
end
