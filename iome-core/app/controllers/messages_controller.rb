class MessagesController < AdminController
  # This helper has been included automatically
  # helper MessageHelper
  def index
    # @messages = Message.order(:created_at).limit(10).offset(0)
    @messages = Message.paginate(page: params[:page])
    @message = Message.new
    # render :index
    # by default this method will call "render :index"
  end

  def new

  end

  def edit

  end

  def show

  end

  def create
    message = Message.new(message_param)
    if message.valid?
      render json: message.save
    else
      render json: message.errors
    end
  end

  def update

  end

  def destroy

  end

  private
  def message_param
    params.require(:message).permit(:content, :status)
  end
end