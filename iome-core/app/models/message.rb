class Message < ApplicationRecord
  enum :status => {
      :backlog => 'backlog',
      :done => 'done',
      :ongoing => 'ongoing',
      :error => 'error',
      :delayed => 'delayed',
      :removed => 'removed'
  }
  validates :content, presence: true, length: { minimum: 2, maximum: 180 }
  # with enum it will raise an error if you pass invalid value, you only need to check its presence
  validates :status, presence: true
  # will_paginate
  self.per_page = 10

  # set per_page globally
  # WillPaginate.per_page = 10
end
