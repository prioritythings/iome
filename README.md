# Technical

## BO:

- Language: Ruby
- Framework: ROR

## Database:

- PostgreSQL

## Mailer & Images

- Amazon S3

## FrontendAPP

- Language: Typescript
- Framework: AngularJS
- Ideas: A portal