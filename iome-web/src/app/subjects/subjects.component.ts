import { Component, OnInit } from '@angular/core';

/**
 * Subjects Component
 *
 * @export
 * @class SubjectsComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.scss']
})

export class SubjectsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
